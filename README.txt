/* $Id: Exp $ */

DESCRIPTION HERE


Installation
------------

Copy the flag_url module to your module directory and then enable on the admin
modules page. 


API
------------

To expose nids to the queueing process, use flag_url_alerts_queue_nids().


/**
 * flag_url_alerts_queue_nids()
 *
 * Expose a group of nodes to the flag url alerts module for inclusion within
 * its alerts queue. Getting a node into the queue consists of two parts:
 * 1 - The node must fall within one of the intervals (daily, weekly, monthly)
 * such that the node creation time plus the interval are less than (i.e. after)
 * the current time. 
 * 2 - Nodes must be returned from an implementation of 
 * _flag_url_alerts_nids_test($nids, $url). With this hook, modules can
 * determine which nids correspond to the given url. (e.g., which nids
 * are returned from a search at the url $url).  
 *
 * Returns array of nids once they have been exposed to all flags in the system
 * This means modules will need to invoke lag_url_alerts_queue_nids() with the
 * same set of nodes until nids are returned from them - may take several passes
 */


Author
------
Chad Fennell
libsys@gmail.com
