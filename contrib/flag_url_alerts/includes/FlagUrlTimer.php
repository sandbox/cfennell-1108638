<?php
// $Id$
/**
 * FlagUrlTimer
 * 
 * Simple class to time script execution
 *
 **/
class FlagUrlTimer {
  private $starttime;
  
  /**
   * Start the timer
   */
  public function start() {
    $mtime = microtime();
    $mtime = explode(' ', $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $this->starttime = $mtime;
  }
  
  /**
   * Stop the timer
   * 
   * @return int
   *  A unix timestamp of the different between start and stop
   */
  public function stop() {
    $mtime = microtime();
    $mtime = explode(" ", $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $endtime = $mtime;
    return $endtime  - $this->starttime;
  }  
}