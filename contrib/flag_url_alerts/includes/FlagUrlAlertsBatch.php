<?php
// $Id$
/**
 * Helper class to iterate over users, flags, etc over successive calls to
 * this script 
 */
class FlagUrlAlertsBatch {

  /**
   * Namespaces allow multiple modules to implement flag url alerts
   *
   * @var string
   */
  private $namespace;

  /**
   * An sql LIMIT that restricts the number of returned results for a given 
   * query
   *
   * @var int
   */
  private $limit;

  /**
   * An sql query string
   *
   * @var string
   */
  private $sql;
  
  /**
   * Constructor - initialize some properies
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   *
   * @param string $sql
   *   An sql query string
   */
  public function FlagUrlAlertsBatch($namespace, $limit = NULL, $sql = NULL) {
    self::namespace_set($namespace);
    self::limit_set($limit);
    self::sql_set($sql);
    self::increment_set($limit);
  }
  
  /**
   * Setter for the $namespace property
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   */
  public function namespace_set($namespace) {
    $this->namespace = $namespace;
  }
  
  /**
   * Setter for the $limit property
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   */
  public function limit_set($limit) {
    $this->limit = $limit;
  }
  
  /**
   * Setter for the $sql property
   *
   * @param string $sql
   *   An sql query string
   */
  public function sql_set($sql) {
    $this->sql = $sql;
  }
  
  /**
   * Getter for the $limit property
   *
   * @return int
   *   The secont number of the LIMIT sql clause
   */ 
  public function limit_get() {
    return variable_get('flag_url_batch_limit_'. $this->namespace, $this->limit);
  }
  
  /**
   * Record the step for a given query 
   * 
   * @param int $limit_step
   *   The first number the sql LIMIT clause 
   */ 
  public function limit_step_set($limit_step) {
    variable_set('flag_url_batch_limit_step_'. $this->namespace, $limit_step);    
  }
  
  /**
   * Fetch the limit step 
   * 
   * @return int
   *   Used as the first number the sql LIMIT clause 
   */ 
  public function limit_step_get() {
    return variable_get('flag_url_batch_limit_step_'. $this->namespace, 0);  
  }
  
  /**
   * Set the number by which to increase each step 
   * 
   * @param int $increment_step
   *   The number by which to increase a step 
   */
  public function increment_set($increment_step) {
    variable_set('flag_url_batch_increment_'. $this->namespace, $increment_step);        
  }
  
  /**
   * Get the number by which to increase each step 
   * 
   * @return int
   *   The number by which to increase a step 
   */
  public function increment_get() {
    return variable_get('flag_url_batch_increment_'. $this->namespace, 0);        
  }
  
  /**
   * Increments a batch to the next step 
   */  
  public function increment() {
    $limit_step = self::limit_step_get();
    $increment = self::increment_get();
    self::limit_step_set($limit_step + $increment);
  }
  
  /**
   * Fetch a batch of results 
   * 
   * @return array
   *   An multidimentional array containing an object array of all flag and
   *   flag urls and a boolean indicating if the current batch is the last
   *   batch of items to be fetched
   */
  public function get_batch() {
    $limit = self::limit_get();
    $limit_step = self::limit_step_get();
    $increment = self::increment_get();

    $results = db_query($this->sql .' LIMIT %d, %d', $limit_step, $limit);        
    
    $batch = array();
    while ($row = db_fetch_object($results)) {
      $batch[] = $row;
    }

    if (empty($batch)) {
      $is_last = TRUE;
      self::limit_step_set(0);
    }
    else {
        // Get the NEXT query step ($limit_step is the LAST step)
        // TODO: label $limit_step as $last_limit_step to make the above 
        // clearer
        $count_limit_step = $limit_step + $increment;
        $count = db_result(db_query($this->sql .' LIMIT %d, %d', $count_limit_step, $limit)); 
        if (!$count) {
          $is_last = TRUE;
          self::limit_step_set(0);
        }
        else {
          self::increment();
        }
    }
    return array('batch' => $batch, 'is_last' => $is_last);
  }
}