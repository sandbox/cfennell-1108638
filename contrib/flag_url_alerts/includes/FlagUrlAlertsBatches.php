<?php
// $Id$
/**
 *  A Utility Class to fetch alert data from the database
 */
class FlagUrlAlertsBatches {

  /**
   * Fetch all flags for the current batch
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   *
   * @return array
   *   An multidimentional array containing an object array of all flag and
   *   flag urls and a boolean indicating if the current batch is the last
   *   batch of items to be fetched
   */
  public static function get_flags($namespace, $limit = NULL) {
    $limit = ($limit !== NULL) ? $limit : 1000;
    $sql = 'SELECT DISTINCT fu.content_id, fu.url FROM {flag_url} fu 
      INNER JOIN {flag_content} fc ON fc.content_id = fu.content_id 
      ORDER BY fu.content_id asc';
    $flags_batch = new flagUrlAlertsBatch('flags_'. $namespace, $limit, $sql);
    return $flags_batch->get_batch();
  }

  /**
   * Fetch a set of batches 
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   *
   * @return array
   *   An array containing an object array of flag and flag urls 
   */
  public static function get_batches($namespace, $limit = NULL) {
    $limit = ($limit !== NULL) ? $limit : 1000;
    $sql = 'SELECT * FROM {flag_url_alerts_batch_queue} ORDER BY alert_interval asc';
    $results = db_query($sql .' LIMIT %d', $limit);
    while ($row = db_fetch_object($results)) {
       $batch[] = $row;
     }
    return  array('batch' => $batch);
  }

  /**
   * Fetch a batch of users with pending alerts
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   *
   * @return array
   *   An multidimentional array containing an object array of user ids and a
   *   boolean indicating if the current batch is the last.
   */
  public static function get_users($namespace, $limit = NULL) {
    $limit = ($limit !== NULL) ? $limit : 500;
    $sql = 'SELECT DISTINCT fua.uid FROM {flag_url_alerts_user_flag_data} fua 
      ORDER BY fua.uid asc';
    $user_batch = new flagUrlAlertsBatch('user_batch_'. $namespace, 50, $sql);
    return $user_batch->get_batch();
  }

  /**
   * Fetch a batch of users with pending alerts
   *
   * @param string $namespace
   *   A string representing the namespace for all flags fetched
   *
   * @param int $limit
   *   An integer representing the maximum number of flags to retrieve
   *
   * @param int $alert_interval
   *   An integer representing the period in seconds (e.g. 86400)  
   * 
   * @param int $content_id
   *   The unique id generated when an item is flagged by a user
   *
   * @return array
   *   An multidimentional array containing an object array of user ids and a
   *   boolean indicating if the current batch is the last.
   */
  public static function get_users_not_attempted($namespace, $limit = NULL, $alert_interval = NULL, $content_id = NULL) {
    $limit = ($limit !== NULL) ? $limit : 500;
    $sql = sprintf('SELECT DISTINCT fua.uid FROM {flag_url_alerts_user_flag_data} fua 
      WHERE fua.alert_interval = %d AND fua.content_id = %d AND
      NOT EXISTS
       (SELECT content_id FROM {flag_url_alerts_batch_attempted} AS fub 
       WHERE fub.content_id = fua.content_id AND fub.alert_interval = fua.alert_interval AND fub.alert_interval = fua.alert_interval)
         LIMIT %d',
         $alert_interval, $content_id, $limit);
     $results = db_query($sql);
     while ($row = db_fetch_object($results)) {
       $batch[] = $row;
     }
     return  array('batch' => $batch);
  }
}