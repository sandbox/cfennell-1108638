Overview
------------

The Flag URL Alerts Module provides a system for regularly sending alerts for large batches of content. The initial goal of this module is to provide search modules with the ability to send users alerts of nodes that are returned from saved searches. 


API
------------

* flag_url_alerts_queue_nid($namespace, $nid, $process_after_date)

Allows modules to submit nodes to the alert system for processing.  $namespaces are stored and useful for making decisions in flag_url_alert hoooks. $process_after_date is a timestamp that indicates when the alert system can 'test' the node against flag_urls. This feature is useful for search modules, such as apachesolr, where there is a delay between indexing time and when the item is available for searching within the index. 

* hook_flag_url_alerts_nids_test($namespace, $nids, $url)

Search modules determine whether or not nids match a given search strategy and return those that do. This hooked is invoked for each unique flag in the system.

* hook_flag_url_alerts_message_items($nids, $url, $account)

Provides a method to theme/format an alert result set. for a given url alert

See the apachesolr_alerts module for implementation examples.

DRUSH Integration
------------------

> drush flag_url_alerts_batch_nids

Run this command to begin processing alerts; defaults to processing up to 1000 flags per request. 

Migrates nids into a queue of batched nids for sending to users.  Nids that are returned from hook_flag_url_alerts_nids_test() for a given flag/url are retained in the batch queue.  Others are discarded.

> drush flag_url_alerts_process_messages

Run this as the final step to send messages to users. Defaults to sending messages to up to 500 users per flag.  Run multiple times if more than 500 users subscribe to a given flag
