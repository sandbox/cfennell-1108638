<?php
// $Id$

/**
 * @file
 * Drush integration for Drupal Queue API.
 */

/**
 * Implementation of hook_drush_help().
 */
function flag_url_alerts_drush_help($section) {
  switch ($section) {
    case 'drush:flag_url_alerts_batch_nids':
      return dt('Batch up nids from alerts queue into message batch queue.');    
    case 'drush:flag_url_alerts_process_messages':
      return dt('Send Flag URL Alert Messages from Batch Queue.');      
  }
}

/**
 * Implementation of hook_queue_drush_command().
 */
function flag_url_alerts_drush_command() {
  $items = array();
  $items['flag_url_alerts_process_messages'] = array(
    'callback' => 'flag_url_alerts_process_messages',
    'description' => 'Send Flag URL Alert Messages from Batch Queue',
    'options' => flag_url_alerts_get_options(),
  );
  $items['flag_url_alerts_batch_nids'] = array(
    'callback' => 'flag_url_alerts_batch_nids_drush',
    'description' => 'Batch up nids from alerts queue into message batch queue.',
    'options' => flag_url_alerts_get_options(),
  );  
  return $items;
}

/**
 * Implementation of hook_get_options() 
 *
 * Get the available options for the "cite_index" drush command.
 *
 * @return
 *    An associative array containing the option definition as the key, and the
 *    description as the value, for each of the available options.
 */
function flag_url_alerts_get_options() {
  $options = array(
   '--LIMIT=<SQL LIMIT>' => dt('Specify a maximum number of items to process (e.g., --LIMIT="1000")'),
  );
  return $options;
}

/**
 * Helper Function to allow batching process to take a drush argument
 */
function flag_url_alerts_batch_nids_drush() {
  $limit = (drush_get_option('LIMIT')) ?drush_get_option('LIMIT') : 1000;
  flag_url_alerts_batch_nids($limit);
}